﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RiseHealth.Domain.Core.Entities;

namespace RiseHealth.Infrastructure.Data
{
    public class RiseHealthContext : DbContext
    {
        public RiseHealthContext(DbContextOptions<RiseHealthContext> options):base(options){}

        private RiseHealthContext(){}

        public DbSet<Patient> Patients { get; set; }
        public DbSet<Professional> Professionals { get; set; }
        public DbSet<Appointment>  Appointments { get; set; }

    }
}
