﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RiseHealth.Domain.Core.Entities;
using RiseHealth.Domain.Core.ValueObjects;

namespace RiseHealth.Infrastructure.Data.Config
{
    public class ProfessionalEntityConfig : IEntityTypeConfiguration<Professional>
    {
        public void Configure(EntityTypeBuilder<Professional> builder)
        {
            builder.ToTable("Professionals");

            builder.HasKey(p => p.Id);

            builder.Property(p => p.Name)
                .IsRequired();

            builder.Property(p => p.Code)
                .ValueGeneratedOnAdd();

        }
    }
}
