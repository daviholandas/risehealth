﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiseHealth.Domain.Core.ValueObjects
{
    public readonly struct Address
    {
        public Address(string street, string avenue, string city, string state, int houseNumber, string zipCode, string country)
        {
            Street = street;
            Avenue = avenue;
            City = city;
            State = state;
            HouseNumber = houseNumber;
            ZipCode = zipCode;
            Country = country;
        }

        public string Street { get; init; }
        public string Avenue { get; init; }
        public string City { get; init; }
        public string State { get; init; }
        public int HouseNumber { get; init; }
        public string ZipCode { get; init; }
        public string Country { get; init; }
    }
}
