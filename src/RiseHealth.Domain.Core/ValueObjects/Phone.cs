﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiseHealth.Domain.Core.ValueObjects
{
    public readonly struct Phone
    {
        public Phone(string number, bool isWhatsapp)
        {
            Number = number;
            IsWhatsapp = isWhatsapp;
        }

        
        public  string Number { get;  init; }
        public bool IsWhatsapp { get; init; }

    }
}
