﻿namespace RiseHealth.Domain.Core.ValueObjects
{
    public struct Council
    {
        public Council(string name, string registrationCode, string estate)
        {
            Name = name;
            RegistrationCode = registrationCode;
            Estate = estate;
        }
        

        public string Name { get; init; }
        public string RegistrationCode { get; init; }
        public string Estate { get; init; }

        public override string ToString()
            => $"{Name} - {RegistrationCode}/{Estate}";
    }
}