﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RiseHealth.Domain.Core.Enums;

namespace RiseHealth.Domain.Core.ValueObjects
{
    public struct Procedure
    {
        public Procedure(int? code, string name, string description, decimal price, decimal deduction, TypeDeduction typeDeduction)
        {
            Code = code;
            Name = name;
            Description = description;
            Price = price;
            Deduction = deduction;
            TypeDeduction = typeDeduction;
        }

        
        public int? Code { get; init; }
        public string Name { get; init; }
        public string Description { get; init; }
        public decimal Price { get; init; }
        public decimal? Deduction { get; init; }
        public TypeDeduction TypeDeduction { get; init; }
    }
}
