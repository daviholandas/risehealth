﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RiseHealth.Domain.Core.Interfaces;

namespace RiseHealth.Domain.Core.Entities
{
    public class Appointment : Entity, IAggregateRoot
    {
        public DateTime Date { get; private set; }
        public Professional Professional { get; private set; }
        public Patient Patient { get; private set; }
        public bool IsConfirmed { get; private set; }
    }
}
