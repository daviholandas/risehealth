﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiseHealth.Domain.Core.Entities
{
    public  abstract class Entity
    {
        public Entity()
        {
            Id = Guid.NewGuid();
            CreatedDate = DateTime.UtcNow;
        }

        public Guid Id { get; private set; }
        public DateTime CreatedDate { get; private set; }
    }
}
