﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RiseHealth.Domain.Core.Interfaces;
using RiseHealth.Domain.Core.ValueObjects;

namespace RiseHealth.Domain.Core.Entities
{
    public class Patient : Entity, IAggregateRoot
    {
        public Patient(string name, string cpf, string rg, DateTime birthDate, string gender, Phone phone, Address address, string healthInsurance)
        {
            Name = name;
            CPF = cpf;
            RG = rg;
            BirthDate = birthDate;
            Gender = gender;
            Phone = phone;
            Address = address;
            HealthInsurance = healthInsurance;
        }

        private Patient(){}

        public string Name { get; private set; }
        public string CPF { get; private set; }
        public string RG { get; private set; }
        public DateTime BirthDate { get; private set; }
        public string Gender { get; private set; }
        public Phone Phone { get; private set; }
        public Address Address { get; private set; }
        public string HealthInsurance { get; private set; }
    }
}
