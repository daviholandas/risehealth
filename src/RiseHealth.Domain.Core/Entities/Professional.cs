﻿using RiseHealth.Domain.Core.Interfaces;
using RiseHealth.Domain.Core.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiseHealth.Domain.Core.Entities
{
    public class Professional : Entity, IAggregateRoot
    {
        public Professional( string name, string photo, DateTime? hiringDate, DateTime? firingDate, Council council, IList<Phone> phones, IList<Procedure> procedures, bool active)
        {
            Name = name;
            Photo = photo;
            HiringDate = hiringDate;
            FiringDate = firingDate;
            Council = council;
            Phones = phones;
            Procedures = procedures;
            Active = active;
        }
        
        private Professional(){}

        public string Name { get; private set; }
        public long Code { get; private set; }
        public string Photo { get; private set; }
        public DateTime? HiringDate { get; private set; }
        public DateTime? FiringDate { get; private set; }
        public Council Council { get; private set; }
        public IList<Phone> Phones { get; private set; }
        public IList<Procedure> Procedures { get; private set; }
        public bool Active { get; private set; }
    }
}
